package drimtim.drivesafereaderapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ConnectActivity extends AppCompatActivity {
    private EditText et_ip;
    private boolean isConnected;
    private HostAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);
        et_ip = findViewById(R.id.ip);
        isConnected = false;
        mAdapter = HostAdapter.getInstance();
    }

    public void buttonClick(View view) {
        try {
            String address = et_ip.getText().toString();
            if (new ConnectSocketTask().execute(new ConnectSocketParameters(address, 12345)).get()) {
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getBaseContext(), "Could not reach host", Toast.LENGTH_SHORT).show();
            }
        }
        catch (Exception e) {
            Toast.makeText(getBaseContext(), "An error occurred", Toast.LENGTH_SHORT).show();
        }
    }
}
