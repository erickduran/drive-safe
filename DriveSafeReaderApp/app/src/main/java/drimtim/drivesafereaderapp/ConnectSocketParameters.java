package drimtim.drivesafereaderapp;

public class ConnectSocketParameters {
    private String hostIp;
    private int port;

    public ConnectSocketParameters(String hostIp, int port) {
        this.hostIp = hostIp;
        this.port = port;
    }

    public String getHostIp() {
        return hostIp;
    }

    public int getPort() {
        return port;
    }
}
