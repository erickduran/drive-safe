package drimtim.drivesafereaderapp;

import android.os.Parcelable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class HostAdapter {
    private static HostAdapter instance;
    private Socket host;
    private InputStream iStream;
    private OutputStream oStream;
    private String hostIp;
    private int port;
    private String id;

    private HostAdapter() {}

    public static HostAdapter getInstance()
    {
        if (instance == null) {
            instance = new HostAdapter();
        }
        return instance;
    }

    public boolean close() throws IOException
    {
        //If the socket is not initialized
        if (host == null) { return false; }

        //If the socket is already closed
        if (host.isClosed()) { return false; }

        //Close everything
        iStream.close();
        oStream.close();
        host.close();
        host = null;
        return true;
    }

    public boolean awaitRequest() throws IOException
    {
        //If the socket is not initialized
        if (host == null) { return false; }

        //If the socket is already closed
        if (host.isClosed()) { return false; }

        //Read the message
        BufferedReader reader = new BufferedReader(new InputStreamReader(iStream));
        String msg = reader.readLine();

        //Keep reading until a scan request is received
        while (!msg.equals("scan request")) {

            //If the message is a ping request, send ack
            if (msg.equals("ping")) {
                send("ok");
            }
            msg = reader.readLine();
        }

        return true;
    }

    protected void send(String message)
    {
        if (host == null) { return; }
        if (host.isClosed()) { return; }

        PrintWriter pw = new PrintWriter(oStream);
        pw.println(message);
        pw.flush();
    }

    public boolean isConnected()
    {

        return host.isConnected();
    }

    public void initializeSocket() throws IOException
    {
        host = new Socket(hostIp, port);
        oStream = host.getOutputStream();
        iStream = host.getInputStream();
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public void setPort(int port) {
        this.port = port;
    }

    //Should only be called after connection is successful
    public void getIdFromServer() throws IOException {
        //Read the message
        BufferedReader reader = new BufferedReader(new InputStreamReader(iStream));
        String msg = reader.readLine();
        id = msg;
    }

    public String getId() { return id; }
}
