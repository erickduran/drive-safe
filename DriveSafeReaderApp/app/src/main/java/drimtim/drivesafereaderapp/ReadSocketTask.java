package drimtim.drivesafereaderapp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

public class ReadSocketTask extends AsyncTask<Void, Void, Boolean> {
    private Activity context;
    private HostAdapter mAdapter;

    public ReadSocketTask(Activity context) {
        this.context = context;
        mAdapter = HostAdapter.getInstance();
    }

    @Override
    public Boolean doInBackground(Void... params)
    {
        try {
            return mAdapter.awaitRequest();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onPostExecute(Boolean params)
    {
        if (params) {
            Intent intent = new Intent(context, ReadActivity.class);
            context.startActivityForResult(intent, 1);
        }
    }
}
