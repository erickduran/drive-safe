package drimtim.drivesafereaderapp;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.NfcF;
import android.os.Bundle;

public class ReadActivity extends Activity {
    PendingIntent pendingIntent;
    IntentFilter[] intentFiltersArray;
    String[][] mTechLists;
    NfcAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        mAdapter = NfcAdapter.getDefaultAdapter(this);

        pendingIntent = PendingIntent.getActivity(
                this,0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);
        IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        intentFiltersArray = new IntentFilter[] {ndef};
        mTechLists = new String[][] { new String[] { NfcF.class.getName() } };
    }

    @Override
    public void onResume() {
        super.onResume();
        //Resume scanning for tags
        if (mAdapter != null) mAdapter.enableForegroundDispatch(this, pendingIntent, intentFiltersArray,
                mTechLists);
    }
    @Override
    public void onNewIntent(Intent intent) {
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        Intent i = new Intent();
        i.putExtra("UID", TagIdParser.getIdString(tag));
        setResult(RESULT_OK, i);
        finish();
    }
    @Override
    public void onPause() {
        //Pause scanning for tags
        super.onPause();
        if (mAdapter != null) mAdapter.disableForegroundDispatch(this);
    }
}
