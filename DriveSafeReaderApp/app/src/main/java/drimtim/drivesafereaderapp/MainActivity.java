package drimtim.drivesafereaderapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    private HostAdapter mAdapter;
    private ReadSocketTask readTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAdapter = HostAdapter.getInstance();
        readTask = new ReadSocketTask(this);
        readTask.execute();
        TextView tvId = findViewById(R.id.reader_id);
        tvId.setText("Reader ID: " + mAdapter.getId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Reconnect to server if connection stopped
        if(!mAdapter.isConnected()) {
            ConnectSocketParameters parameters = null;
            new ConnectSocketTask().execute(parameters);
            new ReadSocketTask(this).execute();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Get data from NFC
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String result = data.getStringExtra("UID");
                Log.i("From main activity", result);

                //Send it to the server
                new SendMessageTask().execute(result);
                Toast.makeText(this, "Sent ID", Toast.LENGTH_SHORT).show();
            }
        }
        //Listen for more requests
        new ReadSocketTask(this).execute();
    }

    public void disconnectClick(View view)
    {
        try {
            if (!readTask.isCancelled()) {
                readTask.cancel(true);
            }
            mAdapter.close();
            this.finish();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}