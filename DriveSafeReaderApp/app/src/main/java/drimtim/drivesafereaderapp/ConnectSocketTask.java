package drimtim.drivesafereaderapp;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ConnectSocketTask extends AsyncTask <ConnectSocketParameters, Void, Boolean> {
    @Override
    protected Boolean doInBackground(ConnectSocketParameters... parameters) {
        HostAdapter mAdapter = HostAdapter.getInstance();
        try {
            if (parameters[0] != null) {
                String hostIp = parameters[0].getHostIp();
                int port = parameters[0].getPort();
                mAdapter.setHostIp(hostIp);
                mAdapter.setPort(port);
            }
            mAdapter.initializeSocket();

            //Get the ID given by the server
            mAdapter.getIdFromServer();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return new Boolean(true);
    }
}
