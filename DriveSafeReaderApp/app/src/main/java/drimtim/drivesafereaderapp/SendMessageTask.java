package drimtim.drivesafereaderapp;

import android.os.AsyncTask;

public class SendMessageTask extends AsyncTask<String, Void, Void> {
    @Override
    protected Void doInBackground(String... params)
    {
        HostAdapter mAdapter = HostAdapter.getInstance();
        try {
            if (mAdapter.isConnected()) {
                mAdapter.send(params[0]);
            }
        } catch (Exception e) {

        }
        return null;
    }
}
