package drimtim.drivesafeapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import drimtim.drivesafeapp.Models.Beverage;

public class BeverageAdapter extends ArrayAdapter<Beverage> {
    public BeverageAdapter(Context context, ArrayList<Beverage> beverages) {
        super(context, 0, beverages);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Beverage b = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout, parent, false);
        }

        //Get views
        TextView tvName = convertView.findViewById(R.id.beverage_name);
        TextView tvContent = convertView.findViewById(R.id.beverage_content);
        TextView tvTimestamp = convertView.findViewById(R.id.beverage_timestamp);

        //Populate
        tvName.setText(b.getName());
        tvContent.setText(String.format("%.0f ml (%.2f ", b.getML(), (b.getAlcohol() * 100)) +  "% alcohol)");
        tvTimestamp.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(b.getDate()));

        return convertView;
    }
}
