package drimtim.drivesafeapp;

import android.nfc.Tag;

public class TagIdParser {
    public static String getIdString(Tag tag)
    {
        StringBuilder sb = new StringBuilder();
        for (String s : byteToString(tag.getId())) {
            sb.append(s);
        }
        return sb.toString();
    }

    private static String[] byteToString(byte[] data)
    {
        String[] result = new String[data.length];
        for(int i = 0; i < data.length; i++) {
            result[i] = String.format("%02x", data[i]);
        }
        return result;
    }
}
