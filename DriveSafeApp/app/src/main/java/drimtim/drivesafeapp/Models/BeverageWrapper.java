package drimtim.drivesafeapp.Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class BeverageWrapper implements Iterable<Beverage>, Serializable {
	private ArrayList<Beverage> beverages;

	private String driverId;

	public BeverageWrapper(ArrayList<Beverage> list) 
	{
		beverages = list;
	}
	
	public Iterator<Beverage> iterator() {
		return beverages.iterator();
	}
	
	public double getAlcohol(){
		double alcohol = 0;
		
		for(int k = 0; k < beverages.size(); k++){
			Beverage b = beverages.get(k);
			alcohol += b.getAlcohol()*b.getML();
		}
		return alcohol / getML();
	}
	public void setDriver( String id ){
		this.driverId = id;
	}
	public double getML(){
		double ml = 0;
		
		for (int i = 0; i < beverages.size(); i++){
			ml += beverages.get(i).getML();
		}
		return ml;
	}

	public ArrayList<Beverage> getBeverages()
	{
		return beverages;
	}
	
}
	