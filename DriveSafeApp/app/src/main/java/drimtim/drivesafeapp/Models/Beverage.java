package drimtim.drivesafeapp.Models;

import java.io.Serializable;
import java.util.Date;

public class Beverage implements Serializable {
	
	private double alcohol;
	private double ml;
	private String name;
	private Date date;
	
	public double getAlcohol(){
		return alcohol;
	}
	
	public double getML(){
		return ml;
	}

	public String getName() {
		return name;
	}
	
	public Date getDate(){
		return date;
	}
	
	public Beverage(String name, double ml, double alcohol, Date date){
		this.name = name;
		this.ml = ml;
		this.alcohol = alcohol;
		this.date = date;
	}


	public String toString(){
		return this.name + " - " + this.ml  + " ml"+ " - " + this.alcohol*100 + " % alcohol " + " - " + this.date; 
	}

}
