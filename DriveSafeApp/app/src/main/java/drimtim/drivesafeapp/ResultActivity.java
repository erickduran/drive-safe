package drimtim.drivesafeapp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import drimtim.drivesafeapp.Models.Beverage;
import drimtim.drivesafeapp.Models.BeverageWrapper;

import static java.lang.Math.round;

public class ResultActivity extends Activity {
    private ListView resultList;
    private TextView tv_summary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        resultList = findViewById(R.id.result_list);
        tv_summary = findViewById(R.id.summary);

        BeverageWrapper wrapper = (BeverageWrapper)getIntent().getSerializableExtra("drinks");
        BeverageAdapter adapter = new BeverageAdapter(this, wrapper.getBeverages());
        resultList.setAdapter(adapter);
        //Empty result
        if(wrapper.getML() == 0) {
            tv_summary.setText("No se encontraron registros");
        } else {
            double ml = wrapper.getML();
            tv_summary.setText("Se han consumido " + (ml>=1000?round(ml)/1000:round(ml)) + " ml a " + round(wrapper.getAlcohol() * 100) + "% de alcohol");
        }
    }
}
