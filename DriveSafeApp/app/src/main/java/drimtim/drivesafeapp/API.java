package drimtim.drivesafeapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import drimtim.drivesafeapp.Models.Beverage;
import drimtim.drivesafeapp.Models.BeverageWrapper;

public class API {
    static private String uri = "https://drivesafe-201701.appspot.com/api/";
    static private String license = "?driver_id=";

    private API(){}

    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    static public Iterable<Beverage> getDrinks( String driverId ){

         BeverageWrapper wrapper = null;

        if(driverId == ""){
            return null;
        }

        URL url = null;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL("https://drivesafe-201701.appspot.com/api/driver" + license + driverId);
            urlConnection = (HttpURLConnection) url.openConnection();


        }catch( Exception e ){
            e.printStackTrace();
        }
        try {
            urlConnection.connect();

            InputStream in = new BufferedInputStream( urlConnection.getInputStream() );
            try {
                JSONObject jsonObj = new JSONObject(getStringFromInputStream(in));
                JSONArray data = jsonObj.getJSONArray("data");

                ArrayList<Beverage> beverages = new ArrayList<>();

                for (int i = 0; i < data.length(); i++) {
                    JSONObject c = data.getJSONObject( i );
                    String name = c.getString("drink_name" );

                    Date date = null;
                    try {
                        date = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ).parse( c.getString( "drink_time" ) );
                    }catch ( ParseException e ){
                        e.printStackTrace();
                    }
                    double alcohol = Double.parseDouble( c.getString( "alcohol" ) );
                    double ml = Double.parseDouble( c.getString( "ml" ) );

                    Beverage tmp = new Beverage( name, ml, alcohol, date );
                    beverages.add( tmp );
                }
                wrapper = new BeverageWrapper( beverages );

            }catch(Exception e){
                //En caso de que no haya JSON
                wrapper = new BeverageWrapper( new ArrayList<Beverage>() );

            }finally {
                wrapper.setDriver(driverId);
            }
            in.close();

        }
        catch( IOException e ) {
            e.printStackTrace();
        }finally {
            urlConnection.disconnect();
        }
        return wrapper;
    }

}
