package drimtim.drivesafeapp;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

import drimtim.drivesafeapp.Models.Beverage;
import drimtim.drivesafeapp.Models.BeverageWrapper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_main);
    }

    public void buttonClick(View view) {
        Intent i = new Intent(view.getContext(), ReadActivity.class);
        startActivityForResult(i, 1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String result = data.getStringExtra("UID");
                Log.i("From main activity", result);

                BeverageWrapper wrapper = (BeverageWrapper) API.getDrinks(result );
                Intent intent = new Intent(this, ResultActivity.class);
                intent.putExtra("drinks", wrapper);
                //Start activity for displaying records
                startActivity(intent);
            }
        }
    }
}
