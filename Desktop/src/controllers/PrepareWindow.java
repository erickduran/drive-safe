package controllers;

import java.io.IOException;
import java.util.ArrayList;

import controllers.buttons.IngredientItemButton;
import controllers.buttons.MenuItemButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.MenuItemWrapper;

public class PrepareWindow {
	
	ArrayList<MenuItemWrapper> cart;
	OrderWindow ow;
	Stage current;
	
	@FXML
	FlowPane scrollboxIngs;
	
	@FXML
	VBox scrollboxCartIngs;
	
	public PrepareWindow(OrderWindow ow, double x, double y) {
		
		try {
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(getClass().getResource("/views/PrepareWindow.fxml"));
	        fxmlLoader.setController(this);

	        Scene scene = new Scene(fxmlLoader.load(), 780, 600);	        
	        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
	        current = new Stage();
	        current.setTitle("DriveSafe");
	        current.setScene(scene);
	        current.setX(x);
	        current.setY(y);
	        current.show();

	    } catch (IOException e) {
	    		new AlertWindow(e.toString());
	    }
		
		cart = new ArrayList<MenuItemWrapper>();
		this.ow = ow;
		this.refreshMenu();
	}
	
	private void refreshMenu() {
		ArrayList<MenuItemWrapper> menu = null;
		
		try {
			menu = MainController.getInstance().getDrinks();
		} catch (Exception e) {
			new AlertWindow(e.toString());
		}
		
		if(menu != null) {
			for(MenuItemWrapper m : menu) {
				MenuItemButton b = new MenuItemButton(m);
				b.setOnMouseClicked(e -> {
					cart.add(m);
					this.refreshCart();
				});
				scrollboxIngs.getChildren().add(b);
			}
		}
	}
	
	private void refreshCart() {
		scrollboxCartIngs.getChildren().clear();
				
		for(MenuItemWrapper m : cart) {
			IngredientItemButton b = new IngredientItemButton(m);
			b.setActionX(e -> {
				cart.remove(m);
				this.refreshCart();
			});
			scrollboxCartIngs.getChildren().add(b);
		}
	}
	
	@FXML
    private void orderButton() {
		if (cart.size() < 1) {
			new AlertWindow("No has preparado tu bebida.");
		}
		else {
			ow.orderCocktail(cart);
		    current.close();
		}
    }
}
