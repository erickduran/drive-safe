package controllers;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class TableInputWindow {
	
	@FXML
	TextField tf_Input;
	
	TableWindow tw;
	Stage current;
	
	public TableInputWindow(TableWindow tw) {
		this.tw = tw;
		try {
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(getClass().getResource("/views/TableInputWindow.fxml"));
	        fxmlLoader.setController(this);

	        Scene scene = new Scene(fxmlLoader.load(), 400, 300);	        
	        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
	        current = new Stage();
	        current.setTitle("DriveSafe");
	        current.setScene(scene);
	        current.show();

	    } catch (IOException e) {
	    		new AlertWindow(e.toString());
	    }
		
		tf_Input.setText("ID");
		tf_Input.selectAll();
	}
	
	@FXML
	private void addButton() {
		int result;
		try {
			result = Integer.parseInt(tf_Input.getText());
		} catch (NumberFormatException e) {
			new AlertWindow("El ID debe ser un número entero.");
			return;
		} 
		
		if(result >= 0) {
			tw.addTable(result);
			current.close();
		}
		else {
			new AlertWindow("El ID debe ser un número positivo.");
		}
	}
	
	@FXML
	private void onEnter() {
		this.addButton();
	}
}
