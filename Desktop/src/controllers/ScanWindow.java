package controllers;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Customer;
import utils.ReaderAppAdapter;

public class ScanWindow {
	
	Customer customer;
	int id;
	Stage current;
	
	ReaderAppAdapter mAdapter;
	
	@FXML
	Label lbl_Title, lbl_Bottom;
	
	public ScanWindow(Customer c, int id) {
		this.id = id;
		this.customer = c;
		try {
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(getClass().getResource("/views/ScanWindow.fxml"));
	        fxmlLoader.setController(this);

	        Scene scene = new Scene(fxmlLoader.load(), 400, 300);	        
	        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
	        current = new Stage();
	        current.setTitle("DriveSafe");
	        current.setScene(scene);
	        current.show();
	        
	        mAdapter  = ReaderAppAdapter.getInstance();

	    } catch (IOException e) {
	    		new AlertWindow(e.toString());
	    }
		
		lbl_Bottom.setText("Por favor escanea en el dispositivo " + id);
	}
	
	public void scan() {
		try {
			customer.setId(mAdapter.requestId(id));
			MainController.getInstance().save();
		} catch (Exception e) {
			new AlertWindow(e.toString());
		}
	}
	
	public void show() {
		current.show();
	}
	
	public void close() {
		current.close();
	}
}
