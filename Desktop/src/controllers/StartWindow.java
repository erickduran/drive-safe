package controllers;

import java.io.IOException;
import java.net.UnknownHostException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import utils.IPHelper;

public class StartWindow {

	@FXML
	Label lbl_Ip;
	
	Stage current;
	
	public StartWindow() {
		try {
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(getClass().getResource("/views/StartWindow.fxml"));
	        fxmlLoader.setController(this);

	        Scene scene = new Scene(fxmlLoader.load(), 800, 500);	        
	        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
	        current = new Stage();
	        current.setTitle("DriveSafe");
	        current.setScene(scene);
	        
	        current.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
	        		if (e.getCode() == KeyCode.ENTER) {
	                startButton();
	             }
	        });
	        
	        current.show();

	    } catch (IOException e) {
	    		new AlertWindow(e.toString());
	    }
		
		String ip = "";
		
		try {
			ip = IPHelper.getLocalHostLANAddress().toString();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		lbl_Ip.setText("Dirección IP: " + ip);
	}
	
	@FXML
    private void startButton() {
		new TableWindow();
	    current.close();
    }

}
