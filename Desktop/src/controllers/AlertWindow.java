package controllers;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class AlertWindow {
	
	@FXML
	Label lbl_Title, lbl_Message;
	
	Stage current;
	
	public AlertWindow(String message) {
		this.setup();
		lbl_Message.setText(message);
	}
	
	public AlertWindow(String title, String message) {
		this.setup();
		lbl_Message.setText(message);
		lbl_Title.setText(title);
	}
	
	public void setup() {
		try {
			// Load view
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(getClass().getResource("/views/AlertWindow.fxml"));
	        fxmlLoader.setController(this);

	        Scene scene = new Scene(fxmlLoader.load(), 400, 300);	        
	        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
	        current = new Stage();
	        current.setTitle("DriveSafe");
	        current.setScene(scene);
	        
	        // Set shortcuts
	        current.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
	        		if (e.getCode() == KeyCode.ENTER || e.getCode() == KeyCode.ESCAPE) {
	                okButton();
	        		}
	        });
	        
	        lbl_Message.getStyleClass().add("ilabel");
	        
	        current.show();

	    } catch (IOException e) {
	    		new AlertWindow(e.toString());
	    }
	}
	
	@FXML
	private void okButton() {
		current.close();
	}
}
