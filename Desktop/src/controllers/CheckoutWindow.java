package controllers;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import model.Customer;

public class CheckoutWindow {
	
	@FXML
	Button btn_Pay;
	
	@FXML
	Label lbl_Total;
	
	OrderWindow ow;
	Customer customer;
	
	Stage current;
	
	public CheckoutWindow(OrderWindow ow, Customer c) {
		this.ow = ow;
		this.customer = c;
		try {
			// Load view
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(getClass().getResource("/views/CheckoutWindow.fxml"));
	        fxmlLoader.setController(this);

	        Scene scene = new Scene(fxmlLoader.load(), 400, 300);	        
	        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
	        current = new Stage();
	        current.setTitle("DriveSafe");
	        current.setScene(scene);
	        
	        // Set shortcuts
	        current.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
	    		if (e.getCode() == KeyCode.ENTER) {
	            payButton();
	         }
	        });
	        
	        current.show();

	    } catch (IOException e) {
	    		new AlertWindow(e.toString());
	    }		
		
		lbl_Total.setText("Total: $" + customer.getTotal());
	}
	
	@FXML
	private void payButton() {
		MainController.getInstance().checkoutCustomer(this.customer);
		ow.refreshTables();
		current.close();
	}
}
