package controllers;

import java.io.IOException;
import java.util.ArrayList;

import controllers.buttons.TableButton;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import model.Table;

public class TableWindow {

	@FXML
	FlowPane flow;
	
	@FXML
	Button btn_Add;
	
	Stage current;
	OrderWindow ow;
	ToggleGroup tablesGroup;
	
	double gap = 10, width = 280, height = 600;
	
	public TableWindow () {
		try {
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(getClass().getResource("/views/TableWindow.fxml"));
	        fxmlLoader.setController(this);

	        Scene scene = new Scene(fxmlLoader.load(), width, height);	        
	        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
	        current = new Stage();
	        current.setTitle("DriveSafe");
	        current.setScene(scene);
	        current.setX(10);
	        
	        current.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
	        		if (e.getCode() == KeyCode.M) {
	                addButton();
	        		}
	        });
	        
	        current.show();

	    } catch (IOException e) {
	    		new AlertWindow(e.toString());
	    }
		
		this.refreshTables();		
	}
	
	@FXML
    public void addButton() {
		new TableInputWindow(this);
	}
	
	public void addTable(int id) {
		Table t = MainController.getInstance().addTable(id);
		this.refreshTables();
		this.selectTable(t);
	}
	
	@FXML
    private void removeButton() {	
		if (tablesGroup.getSelectedToggle() != null) {
			TableButton tb = (TableButton)tablesGroup.getSelectedToggle();
			Table t = tb.getTable();
			
			if (t.getCustomers().size() > 0){
					new AlertWindow("Esta mesa aún tiene clientes.");
			}
			else {
				if (!MainController.getInstance().removeTable(t)) {
					new AlertWindow("No se pudo borrar la mesa.");
				}
				else {
					this.refreshTables();
				}
			}
		}
		else {
			new AlertWindow("Por favor selecciona una mesa.");
		}
	}
	
	public void selectTable(Table t) {
		ObservableList<Node> buttons = flow.getChildren();
		
		for(Node n : buttons) {
			if (((TableButton) n).getTable() == t) {
				((TableButton) n).fire();
			}
		}
	}
	
	public void refreshTables() {
		if(!current.isShowing()) {
			current.show();
		}
		
		flow.getChildren().clear();
		tablesGroup = new ToggleGroup();
		ArrayList<Table> tables = MainController.getInstance().getTables();
		
		for(Table t : tables) {
			TableButton b = new TableButton(t);
			b.setToggleGroup(tablesGroup);
			flow.getChildren().add(b);
		}
				
		tablesGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
				if (tablesGroup.getSelectedToggle() != null) {
					TableButton tb = (TableButton)tablesGroup.getSelectedToggle();
					Table t = tb.getTable();
					
					if (ow == null) {
						ow = new OrderWindow(TableWindow.this, t, current.getX() + width + 20, current.getY());
					}
					else {
						ow.show();
						ow.setTable(t);
						ow.refreshCustomers();
					}
					
					ow.requestFocus();
				}
				else {
					if (ow != null) {
						ow.close();
					}
				}
		    }
		});
	}
	
}
