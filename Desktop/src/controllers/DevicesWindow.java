package controllers;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import model.Customer;
import utils.IPHelper;
import utils.ReaderAppAdapter;

public class DevicesWindow {
	
	Customer customer;
	Stage current;
	OrderWindow ow;
	
	ReaderAppAdapter mAdapter;
	
	@FXML
	Label lbl_Title, lbl_Ip;
	
	@FXML
	FlowPane flow;
	
	public DevicesWindow(OrderWindow ow, Customer c) {
		this.customer = c;
		this.ow = ow;
		try {
			// Load view
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(getClass().getResource("/views/DevicesWindow.fxml"));
	        fxmlLoader.setController(this);

	        Scene scene = new Scene(fxmlLoader.load(), 400, 300);	  
	        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
	        current = new Stage();
	        current.setTitle("DriveSafe");
	        current.setScene(scene);
	        
	        // Set shortcuts
	        current.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
		    		if (e.getCode() == KeyCode.F5) {
		            refreshButton();
		    		}
	        });
	        
	        current.show();
	        
	        // Get reader
	        mAdapter = ReaderAppAdapter.getInstance();

	    } catch (IOException e) {
	    		new AlertWindow(e.toString());
	    }
		
		// Load and show IP
		String ip = null;
		
		try {
			ip = IPHelper.getLocalHostLANAddress().toString();
		} catch (UnknownHostException e) {
			new AlertWindow(e.toString());
		}
		
		lbl_Ip.setText("Dirección IP: " + ip);
		this.refreshDevices();
	}
	
	public void refreshDevices() {
		// Load and show available devices
		flow.getChildren().clear();
		ArrayList<Integer> ids = new ArrayList<Integer>();
		
		try {
			ids = ReaderAppAdapter.getInstance().getConnectedDevicesIndexes();
		} catch (IOException e) {
			new AlertWindow(e.toString());
		}		
		
		for (int id : ids) {
			Button b = new Button(id + "");
			b.getStyleClass().add("ybutton");
			
			b.setOnMouseClicked(e -> {
				ScanWindow s = new ScanWindow(customer, id);
		        Platform.runLater(
		    			new Runnable(){
		        			@Override
		        			public void run(){
		        				// Read and close
		        				current.close();
		        				s.scan();
		        				s.close();
		        				ow.refreshCustomers();
		        			}
		    			});
			});
			
			flow.getChildren().add(b);
		}
	}
	
	@FXML
	private void refreshButton() {
		refreshDevices();
	}
}
