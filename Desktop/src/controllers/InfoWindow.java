package controllers;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Cocktail;
import model.Drink;

public class InfoWindow {
	
	@FXML
	VBox scrollboxItems;
	
	Stage current;

	public InfoWindow(Cocktail cocktail) {
		try {
			// Load view
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(getClass().getResource("/views/InfoWindow.fxml"));
	        fxmlLoader.setController(this);

	        Scene scene = new Scene(fxmlLoader.load(), 400, 500);	        
	        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
	        current = new Stage();
	        current.setTitle("DriveSafe");
	        current.setScene(scene);
	        
	        // Set shortcuts
	        current.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
        			if (e.getCode() == KeyCode.ENTER || e.getCode() == KeyCode.ESCAPE) {
        				okButton();
        			}
	        });
	        
	        current.show();

	    } catch (IOException e) {
	    		new AlertWindow(e.toString());
	    }
		
		Drink c = cocktail;
		
		while(c != null) {
			String ml = String.format("%.2f", c.getIngredientAmount().toMl());
			String alc = String.format("%.2f", c.getIngredientAlcohol()*100);
			Label l = new Label(c.getIngredientName() + " - " + ml + " ml (Alcohol: "+ alc +"%)");
			scrollboxItems.getChildren().add(l);
			c = c.getParent();
		}
	}
	
	@FXML
	private void okButton(){
		current.close();
	}
}
