package controllers;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application{
	public static void main(String[] args) {
		MainController.getInstance();
        launch(args);
    }

	@Override
	public void start(Stage arg0) throws Exception {
		new StartWindow();
	}
}
