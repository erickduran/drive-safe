package controllers;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class CustomerInputWindow {
	
	@FXML
	TextField tf_Input;
	
	OrderWindow ow;
	Stage current;
	
	public CustomerInputWindow(OrderWindow ow) {
		this.ow = ow;
		try {
			// Load view
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(getClass().getResource("/views/CustomerInputWindow.fxml"));
	        fxmlLoader.setController(this);

	        Scene scene = new Scene(fxmlLoader.load(), 400, 300);	        
	        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
	        current = new Stage();
	        current.setTitle("DriveSafe");
	        current.setScene(scene);
	        current.show();

	    } catch (IOException e) {
	    		new AlertWindow(e.toString());
	    }
		
		tf_Input.setText("Cliente");
		tf_Input.selectAll();
	}
	
	@FXML
	private void addButton() {
		ow.addCustomer(tf_Input.getText());
		current.close();
	}
	
	@FXML
	private void onEnter() {
		this.addButton();
	}
}
