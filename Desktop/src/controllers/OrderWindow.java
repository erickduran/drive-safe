package controllers;

import java.io.IOException;
import java.util.ArrayList;

import controllers.buttons.CartItemButton;
import controllers.buttons.CustomerButton;
import controllers.buttons.MenuItemButton;
import controllers.buttons.PrepareItemButton;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Cocktail;
import model.Customer;
import model.MenuItem;
import model.MenuItemWrapper;
import model.Table;

public class OrderWindow {
	
	Stage current;
	
    ToggleGroup customersGroup;
	Table table;
	TableWindow tw;
	
	@FXML
	VBox scrollboxCart, scrollboxCustomers;
	
	@FXML
	FlowPane scrollboxItems;
	
	@FXML
	ScrollPane scrollItems, scrollCart;
		
	@FXML
	Label lbl_Cart, lbl_Items, lbl_Total;
	
	@FXML
	Button btn_Checkout;

	public void setVisibles(boolean b) {
		lbl_Items.setVisible(b);
		scrollItems.setVisible(b);
		
		lbl_Cart.setVisible(b);
		lbl_Total.setVisible(b);
		scrollCart.setVisible(b);
		btn_Checkout.setVisible(b);
	}
	
	public OrderWindow(TableWindow tw, Table t, double x, double y) {
		try {
	        FXMLLoader fxmlLoader = new FXMLLoader();
	        fxmlLoader.setLocation(getClass().getResource("/views/OrderWindow.fxml"));
	        fxmlLoader.setController(this);

	        Scene scene = new Scene(fxmlLoader.load(), 1100, 600);	        
	        scene.getStylesheets().add(getClass().getResource("/styles/styles.css").toExternalForm());
	        current = new Stage();
	        current.setTitle("DriveSafe");
	        current.setScene(scene);
	        current.setX(x);
	        current.setY(y);
	        
	        current.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
	        		if (e.getCode() == KeyCode.C) {
	                addButton();
	             }
	        });
	        
	        current.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
	        		if (e.getCode() == KeyCode.M) {
	                tw.addButton();
	        		}
	        });
	        
	        current.show();

	    } catch (IOException e) {
	    		e.printStackTrace();
	    }
				
		this.table = t;
		this.tw = tw;
		this.refreshMenu();
		this.refreshCustomers();
			
		if(customersGroup.getSelectedToggle() == null) {
			this.setVisibles(false);
		}
	}
	
	public boolean orderCocktail(ArrayList<MenuItemWrapper> items) {
		Customer c = ((CustomerButton)customersGroup.getSelectedToggle()).getCustomer();
		
		try {
			if (!c.order(MainController.getInstance().createItem(items))) {
				new AlertWindow("Por favor agrega la licencia de este cliente.");
				return false;
			}
		} catch (Exception e) {
			new AlertWindow(e.toString());
		}
		refreshCart();
		MainController.getInstance().save();
		return true;
	}
	
	@FXML
    private void addButton() {
        new CustomerInputWindow(this);
    }
	
	@FXML
    private void checkoutButton() {
		Customer c = ((CustomerButton)customersGroup.getSelectedToggle()).getCustomer();
        new CheckoutWindow(this, c);
    }
	
	@FXML
    private void licenseButton() {
		CustomerButton t = (CustomerButton)customersGroup.getSelectedToggle();
		if (t != null) {
			Customer c = (Customer) t.getCustomer();
			new DevicesWindow(this, c);
		}
		else {
			new AlertWindow("Por favor selecciona a un cliente.");
		}
	}
	
	public void addCustomer(String name) {
		Customer c = new Customer(name);
		table.addCustomer(c);
		this.refreshTables();
		MainController.getInstance().save();
		
		ObservableList<Node> buttons = scrollboxCustomers.getChildren();
		
		for (Node n : buttons) {
			CustomerButton cb = (CustomerButton) n;
			if (cb.getCustomer() == c) {
				cb.fire();
			}
		}
	}
	
	public void refreshCustomers() {
		scrollboxCustomers.getChildren().clear();
		customersGroup = new ToggleGroup();
		ArrayList<Customer> customers = this.table.getCustomers();
		for(Customer c : customers) {
			CustomerButton b = new CustomerButton(c);
			b.setToggleGroup(customersGroup);
			scrollboxCustomers.getChildren().add(b);
		}
		
		this.setVisibles(false);
		
		customersGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
			public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
				if (customersGroup.getSelectedToggle() != null) {
					setVisibles(true);
					refreshCart();
		        }
				else {
					setVisibles(false);
				}
		    }
		});
	}
	
	private void refreshMenu() {
		ArrayList<MenuItemWrapper> foods = null;
		ArrayList<MenuItemWrapper> drinks = null;
		ArrayList<MenuItemWrapper> cocktails = null;
		
		try {
			foods = MainController.getInstance().getFoods();
			drinks = MainController.getInstance().getDrinks();
			cocktails = MainController.getInstance().getCocktails();
		} catch (Exception e) {
			new AlertWindow(e.toString());
		}
		
		if(foods != null && drinks != null && cocktails != null) {
			
			// Prepare button
			PrepareItemButton p = new PrepareItemButton();
			
			p.setOnMouseClicked(e -> {
				this.prepareButton();
			});
			
			scrollboxItems.getChildren().add(this.getDivider("Cocteles"));
			scrollboxItems.getChildren().add(p);
			this.loadItems(cocktails);
			
			scrollboxItems.getChildren().add(this.getDivider("Bebidas"));
			this.loadItems(drinks);
			
			scrollboxItems.getChildren().add(this.getDivider("Comida"));
			this.loadItems(foods);
			
		}
		else {
			new AlertWindow("No se pudo cargar el menú");
		}
	}
	
	private void refreshCart() {
		scrollboxCart.getChildren().clear();
		
		Customer c = ((CustomerButton)customersGroup.getSelectedToggle()).getCustomer();
		ArrayList<MenuItem> items = c.getItems();
		
		for(MenuItem m : items) {
			CartItemButton b = new CartItemButton(m);
			b.setOnMouseClicked(e -> {
				if(m instanceof Cocktail) {
					new InfoWindow((Cocktail) m);
				}
			});
			
			b.setActionX(e -> {
				c.remove(m);
				this.refreshCart();
				MainController.getInstance().save();
			});
			
			scrollboxCart.getChildren().add(b);
		}
		
		lbl_Total.setText("Total: $" + c.getTotal());
	}
	
    private void prepareButton() {
		new PrepareWindow(this, current.getX(), current.getY());
    }
	
	public void setTable(Table t) {
		this.table = t;
	}
	
	public void close() {
		this.current.close();
	}
	
	public void show() {
		this.current.show();
	}
	
	public void requestFocus() {
		this.current.requestFocus();
	}
	
	public void refreshTables() {
		tw.refreshTables();
		tw.selectTable(this.table);
	}
	
	@FXML
    private void removeButton() {	
		if (customersGroup.getSelectedToggle() != null) {
			CustomerButton cb = (CustomerButton)customersGroup.getSelectedToggle();
			Customer c = cb.getCustomer();
			
			if (c.getItems().size() > 0){
				new AlertWindow("Este cliente aún tiene artículos sin pagar.");
			}
			else {
				if (!table.removeCustomer(c)) {
					new AlertWindow("No se pudo borrar la mesa.");
				}
				else {
					this.refreshTables();
					MainController.getInstance().save();
				}
			}
		}
		else {
			new AlertWindow("Por favor selecciona a un cliente.");
		}
	}
	
	private HBox getDivider(String text) {
		HBox box = new HBox();
		box.setPrefWidth(400);
		box.setPrefHeight(50);
		box.setAlignment(Pos.CENTER_LEFT);
		
		Label l = new Label (text);
		l.getStyleClass().add("divider-label");
		box.getChildren().add(l);
		return box;
	}
	
	private void loadItems(ArrayList<MenuItemWrapper> menu) {
		for(MenuItemWrapper m : menu) {				
			MenuItemButton b = new MenuItemButton(m);
			
			b.setOnMouseClicked(e -> {
				Customer c = ((CustomerButton)customersGroup.getSelectedToggle()).getCustomer();		
					
				try {
					if (!c.order(MainController.getInstance().createItem(m.getId()))) {
						new AlertWindow("Por favor agrega la licencia de este cliente.");
					}
					else {
						refreshCart();
						MainController.getInstance().save();
					}
				} catch (Exception ex) {
					new AlertWindow(ex.toString());
				}
			});
			
			scrollboxItems.getChildren().add(b);
		}
	}
}
