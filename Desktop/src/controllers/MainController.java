package controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import model.Customer;
import model.MenuItem;
import model.MenuItemWrapper;
import model.Table;
import utils.MenuItemFactory;
import utils.ReaderAppAdapter;
import utils.Saver;

public class MainController {
	
	static MainController instance = null;
	ReaderAppAdapter adapter;
	MenuItemFactory menuItems;
	ArrayList<Table> tables = new ArrayList<Table>();
	
	protected MainController() {
		
		menuItems = MenuItemFactory.getInstance();
		tables = Saver.load();
		if (tables == null) {
			tables = new ArrayList<Table>();
		}
		
		try {
			adapter = ReaderAppAdapter.getInstance();
		} catch (IOException e) {
			new AlertWindow(e.toString());
		}
		
        Thread t = new Thread() {
        		public void run() {
        			while (true) {
        				try {
						adapter.connect();
        				} catch (IOException e) {
        					new AlertWindow(e.toString());
        				}
        			}
        		}
        };
        
        t.setDaemon(true);
        t.start();
	}
	
	public static MainController getInstance() {
		if(instance == null) {
			instance = new MainController();
		}
		return instance;
	}
	
	public ArrayList<MenuItemWrapper> getMenu() throws ParserConfigurationException, IOException, SAXException {
		return menuItems.getMenu();
	}
	
	public ArrayList<MenuItemWrapper> getDrinks() throws ParserConfigurationException, IOException, SAXException {
		return menuItems.getDrinks();
	}
	
	public ArrayList<MenuItemWrapper> getFoods() throws ParserConfigurationException, IOException, SAXException {
		return menuItems.getFoods();
	}
	
	public ArrayList<MenuItemWrapper> getCocktails() throws ParserConfigurationException, IOException, SAXException {
		return menuItems.getCocktails();
	}
	
	public ArrayList<Table> getTables(){
		return tables;
	}
	
	public void checkoutCustomer(Customer c) {
		for (Table t : tables) {
			if (t.contains(c)) {
				t.checkout(c);
				break;
			}
		}
		this.save();
	}
	
	public ReaderAppAdapter getAdapter() {
		return this.adapter;
	}
	
	public MenuItem createItem(String s) throws ParserConfigurationException, IOException, SAXException{
		return menuItems.order(s);
	}
	
	public MenuItem createItem(ArrayList<MenuItemWrapper> items) throws ParserConfigurationException, IOException, SAXException{
		return menuItems.orderCocktail(items);
	}
	
	public Table addTable(int id) {
		Table t = new Table(id);
		tables.add(t);
		this.save();
		return t;
	}
	
	public boolean removeTable(Table t) {
		boolean result = this.tables.remove(t);
		this.save();
		return result;
	}
	
	public void save() {
		Saver.save(tables);
	}
}
