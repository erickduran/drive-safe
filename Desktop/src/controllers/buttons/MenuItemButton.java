package controllers.buttons;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.DrinkWrapper;
import model.MenuItemWrapper;

public class MenuItemButton extends Button {
	
	MenuItemWrapper item;
	
	Label title, price;
	ImageView iv;
	Image image;
	
	public MenuItemButton(MenuItemWrapper m) {
		this.item = m;
		HBox layout = new HBox();
		VBox items = new VBox();
		
		// Set title and price along with format
		title = new Label(m.getName());
		price = new Label("$" + m.getPrice());
		title.getStyleClass().add("menuitem-title");
		price.getStyleClass().add("menuitem-text");
		
		items.getChildren().addAll(title, price);
		items.setAlignment(Pos.CENTER_LEFT);
		
		HBox box = new HBox();

		// Prepare and set image
		image = new Image(m.getImage());
		iv = new ImageView(image);
		iv.setFitHeight(50);
		iv.setPreserveRatio(true);
		iv.setCache(true);
		box.getChildren().add(iv);
		box.setAlignment(Pos.CENTER);
		box.setPrefWidth(55);
		
		// Set layout
		layout.setSpacing(15);
		layout.setAlignment(Pos.CENTER_LEFT);
		layout.getChildren().addAll(box, items);
		
		this.setGraphic(layout);
		this.getStyleClass().add("menuitem");
		
		// Set tooltips (hover)
		if (m instanceof DrinkWrapper) {
			String qty = String.format("%.2f", ((DrinkWrapper)m).getQuantity());
			this.setTooltip(new Tooltip(((DrinkWrapper)m).getName() + " (" + qty + " " + ((DrinkWrapper)m).getUnit() + ")"));
		}
		else {
			this.setTooltip(new Tooltip(((MenuItemWrapper)m).getName()));
		}
		
		// On hover behavior
		this.setOnMouseEntered(e -> {
			title.getStyleClass().remove("menuitem-title");
			price.getStyleClass().remove("menuitem-text");
			title.getStyleClass().add("menuitem-title-h");
			price.getStyleClass().add("menuitem-text-h");
		});
		
		this.setOnMouseExited(e -> {
			title.getStyleClass().remove("menuitem-title-h");
			price.getStyleClass().remove("menuitem-text-h");
			title.getStyleClass().add("menuitem-title");
			price.getStyleClass().add("menuitem-text");
		});
	}
	
	public MenuItemWrapper getItem() {
		return this.item;
	}
}
