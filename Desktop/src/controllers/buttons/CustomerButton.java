package controllers.buttons;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import model.Customer;

public class CustomerButton extends ToggleButton {
	
	Customer customer;
	
	String imagePath = "/assets/buttons/license-w.png";
	String imagePath_h = "/assets/buttons/license-b.png";
	
	boolean isInside;
	
	Label name;
	HBox layout;
	
	ImageView iv = new ImageView(new Image(imagePath));
	ImageView iv_h = new ImageView(new Image(imagePath_h));
	
	public CustomerButton(Customer c) {
		this.customer = c;
		layout = new HBox();
		
		// Set title
		name = new Label(c.getDescription());
		name.getStyleClass().add("menuitem-title");
		name.setPrefWidth(145);
		
		// Prepare image (normal)
		iv.setFitWidth(20);
		iv.setPreserveRatio(true);
		iv.setCache(true);
		
		// Prepare image (hover)
		iv_h.setFitWidth(20);
		iv_h.setPreserveRatio(true);
		iv_h.setCache(true);
		
		// Prepare layout
		layout.setSpacing(15);
		layout.setAlignment(Pos.CENTER_LEFT);
		layout.getChildren().add(name);
		
		// Show license icon
		if (c.isScanned()) {
			layout.getChildren().add(iv);
		}
		
		this.setGraphic(layout);
		this.getStyleClass().add("cbutton");
		
		// On hover behavior (check license)
		this.setOnMouseEntered(e -> {
			name.getStyleClass().remove("menuitem-title");
			name.getStyleClass().add("menuitem-title-h");
			if(c.isScanned()) {
				this.changeImage(iv_h);
			}
			isInside = true;
		});
		
		this.setOnMouseExited(e -> {
			name.getStyleClass().remove("menuitem-title-h");
			name.getStyleClass().add("menuitem-title");
			isInside = false;
			if(!this.isSelected() && c.isScanned()) {
				this.changeImage(iv);
			}
		});
		
		// On selected behavior (check license)
		this.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
				if (isSelected()) {				
					name.getStyleClass().remove("menuitem-title");
					name.getStyleClass().add("menuitem-title-h");
					if(customer.isScanned()) {
						changeImage(iv_h);
					}
				}
				else {
					name.getStyleClass().remove("menuitem-title-h");
					name.getStyleClass().add("menuitem-title");
					if(!isInside && customer.isScanned()) {
						changeImage(iv);
					}
				}
			}
		});
				
	}
	
	private void changeImage(ImageView i) {
		// Image switch help
		layout.getChildren().clear();
		layout.getChildren().addAll(name, i);
		this.setGraphic(layout);
	}
	
	public Customer getCustomer() {
		return this.customer;
	}
}

