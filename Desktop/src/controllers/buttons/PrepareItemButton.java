package controllers.buttons;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class PrepareItemButton extends Button {
	
	Label title = new Label("Preparar");
	String path = "/assets/menu/cocktail.png";
	
	ImageView iv;
	Image image;
	
	public PrepareItemButton() {
		HBox layout = new HBox();
		VBox items = new VBox();
		
		// Set title
		title.getStyleClass().add("menuitem-title");
		
		items.getChildren().add(title);
		items.setAlignment(Pos.CENTER_LEFT);
		
		HBox box = new HBox();

		// Prepare image
		image = new Image(path);
		iv = new ImageView(image);
		iv.setFitHeight(50);
		iv.setPreserveRatio(true);
		iv.setCache(true);
		box.getChildren().add(iv);
		box.setAlignment(Pos.CENTER);
		box.setPrefWidth(55);
		
		// Set layout
		layout.setSpacing(15);
		layout.setAlignment(Pos.CENTER_LEFT);
		layout.getChildren().addAll(box, items);
		
		this.setGraphic(layout);
		this.getStyleClass().add("menuitem");
		this.setTooltip(new Tooltip("Prepara tu propia bebida"));
		
		// On hover behavior
		this.setOnMouseEntered(e -> {
			title.getStyleClass().remove("menuitem-title");
			title.getStyleClass().add("menuitem-title-h");
		});
		
		this.setOnMouseExited(e -> {
			title.getStyleClass().remove("menuitem-title-h");
			title.getStyleClass().add("menuitem-title");
		});
	}
}
