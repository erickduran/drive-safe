package controllers.buttons;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import model.Drink;
import model.MenuItem;

public class CartItemButton extends Button {
	
	MenuItem item;
	
	Label title, price;
	Button xButton;
	
	public CartItemButton(MenuItem m) {
		this.item = m;
		HBox layout = new HBox();
		
		// Load title
		title = new Label(m.getName());
		title.getStyleClass().add("cartitem-text");
		title.setPrefWidth(90);
		
		// Load price
		price = new Label("$" + m.getPrice());
		price.getStyleClass().add("cartitem-text");
		price.setPrefWidth(50);
		
		// Create remove button
		xButton = new Button();
		xButton.getStyleClass().clear();
		xButton.getStyleClass().add("xbutton");
		
		// Set positions and layout
		layout.setSpacing(15);
		layout.setAlignment(Pos.CENTER_LEFT);
		layout.getChildren().addAll(title, price, xButton);
		
		this.setGraphic(layout);
		this.getStyleClass().add("cartitem");
		
		// Set tooltips (hover)
		if (m instanceof Drink) {
			String ml = String.format("%.2f", ((Drink)m).getAmount().toMl());
			String alc = String.format("%.2f", ((Drink)m).getAlcohol());
			this.setTooltip(new Tooltip(((Drink)m).getName() + " (" + ml + "ml " + alc + "%)"));
		}
		else {
			this.setTooltip(new Tooltip(((MenuItem)m).getName()));
		}
		
		// On hover behavior
		this.setOnMouseEntered(e -> {
			title.getStyleClass().remove("cartitem-text");
			price.getStyleClass().remove("cartitem-text");
			xButton.getStyleClass().remove("xbutton");
			title.getStyleClass().add("cartitem-text-h");
			price.getStyleClass().add("cartitem-text-h");
			xButton.getStyleClass().add("xbutton-h");
		});
		
		this.setOnMouseExited(e -> {
			title.getStyleClass().remove("cartitem-text-h");
			price.getStyleClass().remove("cartitem-text-h");
			xButton.getStyleClass().remove("xbutton-h");
			title.getStyleClass().add("cartitem-text");
			price.getStyleClass().add("cartitem-text");
			xButton.getStyleClass().add("xbutton");
		});
				
	}
	
	public MenuItem getItem() {
		return this.item;
	}
	
	public void setActionX(EventHandler<? super MouseEvent> e) {
		xButton.setOnMouseClicked(e);
	}
}
