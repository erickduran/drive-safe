package controllers.buttons;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import model.MenuItemWrapper;

public class IngredientItemButton extends Button {
	
	MenuItemWrapper item;
	
	Label title, price;
	Button xButton;
	
	public IngredientItemButton(MenuItemWrapper m) {
		this.item = m;
		HBox layout = new HBox();
		
		// Set title
		title = new Label(m.getName());
		title.getStyleClass().add("cartitem-text");
		title.setPrefWidth(90);
		
		// Set price
		price = new Label("$" + m.getPrice());
		price.getStyleClass().add("cartitem-text");
		price.setPrefWidth(50);
		
		// Create remove button
		xButton = new Button();
		xButton.getStyleClass().clear();
		xButton.getStyleClass().add("xbutton");
		
		// Set positions and layout
		layout.setSpacing(15);
		layout.setAlignment(Pos.CENTER_LEFT);
		layout.getChildren().addAll(title, price, xButton);
		
		this.setGraphic(layout);
		this.getStyleClass().add("cartitem");
		
		// On hover behavior
		this.setOnMouseEntered(e -> {
			title.getStyleClass().remove("cartitem-text");
			price.getStyleClass().remove("cartitem-text");
			xButton.getStyleClass().remove("xbutton");
			title.getStyleClass().add("cartitem-text-h");
			price.getStyleClass().add("cartitem-text-h");
			xButton.getStyleClass().add("xbutton-h");
			
		});
		
		this.setOnMouseExited(e -> {
			title.getStyleClass().remove("cartitem-text-h");
			price.getStyleClass().remove("cartitem-text-h");
			xButton.getStyleClass().remove("xbutton-h");
			title.getStyleClass().add("cartitem-text");
			price.getStyleClass().add("cartitem-text");
			xButton.getStyleClass().add("xbutton");
		});
				
	}
	
	public MenuItemWrapper getItem() {
		return this.item;
	}
	
	public void setActionX(EventHandler<? super MouseEvent> e) {
		xButton.setOnMouseClicked(e);
	}
}
