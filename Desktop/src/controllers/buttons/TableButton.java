package controllers.buttons;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.VBox;
import model.Table;

public class TableButton extends ToggleButton {
	
	Table table;
	
	Label top, bottom;
	
	public TableButton(Table t) {
		this.table = t;
		VBox items = new VBox();
		
		// Set number and id texts
		top = new Label(table.getSize() + "");
		bottom = new Label("Mesa " + t.getId());
		top.getStyleClass().add("table-number");
		bottom.getStyleClass().add("table-name");
		
		items.getChildren().addAll(top, bottom);
		items.setAlignment(Pos.CENTER);
		this.setGraphic(items);
		this.getStyleClass().add("tbutton");
		
		// On hover behavior
		this.setOnMouseEntered(e -> {
			top.getStyleClass().remove("table-number");
			bottom.getStyleClass().remove("table-name");
			top.getStyleClass().add("table-number-h");
			bottom.getStyleClass().add("table-name-h");
		});
		
		this.setOnMouseExited(e -> {
			top.getStyleClass().remove("table-number-h");
			bottom.getStyleClass().remove("table-name-h");
			top.getStyleClass().add("table-number");
			bottom.getStyleClass().add("table-name");
		});
		
		// On selected behavior
		this.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
				if (isSelected()) {				
					top.getStyleClass().remove("table-number");
					bottom.getStyleClass().remove("table-name");
					top.getStyleClass().add("table-number-h");
					bottom.getStyleClass().add("table-name-h");
				}
				else {
					top.getStyleClass().remove("table-number-h");
					bottom.getStyleClass().remove("table-name-h");
					top.getStyleClass().add("table-number");
					bottom.getStyleClass().add("table-name");
				}
			}
		});
	}
	
	public Table getTable() {
		return this.table;
	}
}
