package utils;

import java.util.ArrayList;

import controllers.AlertWindow;
import model.Cocktail;
import model.CocktailWrapper;
import model.Drink;
import model.DrinkWrapper;
import model.Food;
import model.FoodWrapper;
import model.Measure;
import model.MenuItem;
import model.MenuItemWrapper;

public class MenuItemFactory {
    private final String file = "src/assets/Menu.xml";
    private static MenuItemFactory instance;
    private ArrayList<MenuItemWrapper> menu;
	
	private MenuItemFactory() { }
	
	public static MenuItemFactory getInstance() {
		if (instance == null) {
            instance = new MenuItemFactory();
		}
		return instance;
    }
    
    private boolean loadMenu() {
	    	try {
	    		menu = MenuParser.parseXMLMenu(file);
	    		return true;
	    	} catch (Exception e) {
	    		new AlertWindow(e.toString());
			return false;
		}
    }

    public ArrayList<MenuItemWrapper> getMenu() {
        if (menu == null) { 
            loadMenu();
        }
        return menu;
    }

    public ArrayList<MenuItemWrapper> getDrinks() {
        if (menu == null) {
            loadMenu();
        }
        ArrayList<MenuItemWrapper> temp = new ArrayList<MenuItemWrapper>();
        for (MenuItemWrapper item : menu) {
            if (item instanceof DrinkWrapper) {
                temp.add(item);
            }
        }
        return temp;
    }
    
    public ArrayList<MenuItemWrapper> getFoods() {
        if (menu == null) {
            loadMenu();
        }
        ArrayList<MenuItemWrapper> temp = new ArrayList<MenuItemWrapper>();
        for (MenuItemWrapper item : menu) {
            if (item instanceof FoodWrapper) {
                temp.add(item);
            }
        }
        return temp;
    }
    
    public ArrayList<MenuItemWrapper> getCocktails() {
        if (menu == null) {
            loadMenu();
        }
        ArrayList<MenuItemWrapper> temp = new ArrayList<MenuItemWrapper>();
        for (MenuItemWrapper item : menu) {
            if (item instanceof CocktailWrapper) {
                temp.add(item);
            }
        }
        return temp;
    }
	
	public MenuItem order(String itemId) 
	{
        //Load it only the first time it's needed
        if (menu == null) { 
        		loadMenu();
        }
        
        for(MenuItemWrapper temp : menu) {
            if (temp.getId().equals(itemId)) {
                if (temp instanceof DrinkWrapper) {
                    return new Drink(temp.getName(), temp.getPrice(), new Measure(((DrinkWrapper)temp).getQuantity(), ((DrinkWrapper)temp).getUnit()), ((DrinkWrapper)temp).getAlcohol());
                } 
                else if (temp instanceof CocktailWrapper) {
                	Cocktail c = (Cocktail) orderCocktail(((CocktailWrapper)temp).getIngredients());
                	if (c != null) {
                		c.setCocktailName(((CocktailWrapper)temp).getName());
                	}
                	return c;
                }
                else {
                    return new Food(temp.getName(), temp.getPrice());
                }
            }
        }

        return null;
    }
	
	public MenuItem addToCocktail(String itemId, Drink parent) {
		if (menu == null) {
			System.out.println("Loading menu");
			loadMenu();
		}
		
		MenuItem item = null;
		for (MenuItemWrapper temp : menu) {
			if (temp.getId().equals(itemId)) {
				return new Cocktail(temp.getName(), temp.getPrice(), new Measure(((DrinkWrapper)temp).getQuantity(), ((DrinkWrapper)temp).getUnit()), ((DrinkWrapper)temp).getAlcohol(), parent);
			}
		}
		
		return item;
	}
	
	public MenuItem orderCocktail(ArrayList<MenuItemWrapper> items) {
		MenuItem d = null;
		for (int i = 0; i < items.size(); i++) {
			if (i == 0) { //First element
				d = order(items.get(i).getId());
				if (d == null) {
					return null;
				}
				continue;
			}
			
			d = addToCocktail(items.get(i).getId(), (Drink)d);
			if (d == null) {
				return null;
			}
		}
		
		return d;
	}
	
	
}
