package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import controllers.AlertWindow;
import model.Table;

public class Saver {
	
	private static String filename = "src/assets/save.ser";
	
	public static void save(ArrayList<Table> tables) {
		if(Saver.exist()) {
			try {
		        FileOutputStream fos = new FileOutputStream(filename);
		        ObjectOutputStream oos = new ObjectOutputStream(fos);
		        oos.writeObject(tables);
		        oos.close();
			}catch(Exception e) {
				new AlertWindow(e.toString());
	        	}
		}
		else {
			Saver.create();	
		}
	}

	private static void create() {
        try {
	        	FileOutputStream fos = new FileOutputStream(filename);
	        	ObjectOutputStream oos = new ObjectOutputStream(fos);
	        	oos.writeObject(new ArrayList<Table>());
	        	oos.close();
        }
        catch(Exception e) {
        		new AlertWindow(e.toString());
        }
	}

	private static boolean exist() {
		File f = new File(filename);
		if (f.exists()) {
			return true;
		}
		return false;
	}
	
	public static ArrayList<Table> load() {
		try {
			if (!exist()) {
				System.out.println("no");
				return null;
			}
			FileInputStream fis = new FileInputStream(filename);
			ObjectInputStream ois = new ObjectInputStream(fis);
			ArrayList<Table> readObject = (ArrayList<Table>) ois.readObject();
			ois.close();
			return readObject;
		}catch(Exception e) {
			new AlertWindow(e.toString());
		}
		return null;
	}
}
