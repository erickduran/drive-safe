package utils;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import controllers.AlertWindow;

public class ReaderAppAdapter
{
    private static ReaderAppAdapter instance;
    private InetAddress ipAddr;
    private ServerSocket server;
    private ArrayList<Socket> clients;

    private ReaderAppAdapter() throws IOException
    { 
        ipAddr = IPHelper.getLocalHostLANAddress();
        server = new ServerSocket(12345, 1, ipAddr);
        clients = new ArrayList<Socket>();
    }

    public static ReaderAppAdapter getInstance() throws IOException
    {
        if (instance == null) {
            instance = new ReaderAppAdapter();
        }
        return instance;
    }

    public String getAddress()
    {
        return ipAddr.getHostAddress();
    }

    public boolean connect() throws IOException
    {
        Socket client = server.accept();
        clients.add(client);
        PrintWriter pw = new PrintWriter(client.getOutputStream());
        pw.println(clients.indexOf(client)); //Send the reader ID to the client
        pw.flush();
        return true;
    }

    public String requestId(int id) throws Exception
    {
    		Socket client = clients.get(id);
    		OutputStream oStream = client.getOutputStream();
        PrintWriter pw = new PrintWriter(oStream);
        pw.println("scan request");
        pw.flush();
        return read(id);
    }

    public String read(int id) throws IOException
    {
    		Socket client = clients.get(id);
    		InputStream iStream = client.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(iStream));
        String result = reader.readLine();
        return result;
    }

    public void closeConnection(int id) throws IOException
    {
	    	Socket client = clients.get(id);
	    	OutputStream oStream = client.getOutputStream();
	    	InputStream iStream = client.getInputStream();
        PrintWriter pw = new PrintWriter(oStream);
        pw.println("end");
        pw.flush();
        iStream.close();
        oStream.close();
        client.close();
        server.close();
    }
    
    public boolean isConnected(int id)
    {
    		try {
            Socket client = clients.get(id);

            client.setSoTimeout(500); //Set a timeout for ping in ms
            OutputStream oStream = client.getOutputStream();
            PrintWriter pw = new PrintWriter(oStream);
            pw.println("ping");
            pw.flush();
            
            if(!read(id).equals("ok")) {
                client.setSoTimeout(0);
                return false;
            }
            
            client.setSoTimeout(0);
            return true;
    		} catch (Exception e) { //If the host is not reachable, if timeout expires or if message is null
            return false;
    		}
    }

    public ArrayList<Integer> getConnectedDevicesIndexes() {
        ArrayList<Integer> connectedDevices = new ArrayList<Integer>();

        // Thread solution; comment if doesn't work
        ArrayList<Thread> threads = new ArrayList<Thread>();

        for (int i = 0; i < clients.size(); i++) {
            final int id = i;
            final Object lock = new Object();
            Thread t = new Thread() {
                public void run() {
                    if (isConnected(id)) {
                        synchronized (lock) {
                            connectedDevices.add(id);
                        }
                    }
                }
            };
            threads.add(t);
            t.start();
        }

        for (Thread t : threads) { 
            try {
                t.join();
            } catch(InterruptedException e) {
            		new AlertWindow(e.toString());
            }
        }

        return connectedDevices;
    }
}