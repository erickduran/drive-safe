package utils;

import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import model.CocktailWrapper;
import model.DrinkWrapper;
import model.FoodWrapper;
import model.MenuItemWrapper;
import model.Unit;

import java.io.*;
import java.util.ArrayList;

public class MenuParser
{
    public static ArrayList<MenuItemWrapper> parseXMLMenu(String file) throws ParserConfigurationException, IOException, SAXException {
        ArrayList<MenuItemWrapper> items = new ArrayList<MenuItemWrapper>();
        
        //Read file
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(file);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("item");


        //Iterate through each element and add it to the collection
        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                //Get menu item properties
                Element item = (Element) node;
                
                //Skip if the item is a cocktail
                if (item.getAttribute("kind").equals("cocktail")) { 
                	continue;
                }
                
                String id = item.getAttribute("id");
                String name = item.getElementsByTagName("name").item(0).getTextContent();
                double price = Double.parseDouble(item.getElementsByTagName("price").item(0).getTextContent());
                String image = item.getElementsByTagName("image").item(0).getTextContent();

                //Get drink related properties
                if (item.getAttribute("kind").equals("drink")) {
                    double alcohol = Double.parseDouble(item.getElementsByTagName("alcohol").item(0).getTextContent());
                    double quantity = Double.parseDouble(item.getElementsByTagName("quantity").item(0).getTextContent());
                    Unit unit = ((Element) item.getElementsByTagName("quantity").item(0)).getAttribute("unit").equals("oz") ? Unit.OZ : Unit.ML;
                    items.add(new DrinkWrapper(id, name, price, alcohol, quantity, unit, image));
                }

                //Food
                else if (item.getAttribute("kind").equals("food")){ 
                    items.add(new FoodWrapper(id, name, price, image));
                }
            }
        }
        
        //After menu is loaded, get cocktail recipes and return it        
        return parseXMLCocktails(file, items);
    }
    
    private static ArrayList<MenuItemWrapper> parseXMLCocktails(String file, ArrayList<MenuItemWrapper> menu) throws ParserConfigurationException, IOException, SAXException {
    		ArrayList<MenuItemWrapper> cocktails = new ArrayList<MenuItemWrapper>();
    		int cont = 0;
    	
        //Read file
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(file);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("item");


        //Iterate through each element and add it to the collection
        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                //Get menu item properties
                Element item = (Element) node;
                
                //Skip if it's not a cocktail
                if (!item.getAttribute("kind").equals("cocktail")) { 
                	continue;
                }
                
                //Get attributes
                String id = item.getAttribute("id");
                String name = item.getAttribute("name");
                ArrayList<MenuItemWrapper> ingredientList = new ArrayList<MenuItemWrapper>();
                double price = 0;
                String image = item.getElementsByTagName("image").item(0).getTextContent();
                
                //Get the cocktail ingredients and price
                NodeList ingredients = item.getElementsByTagName("ingredient");
                for (int j = 0; j < ingredients.getLength(); j++) {
                    Node ingNode = ingredients.item(j);
                    if (ingNode.getNodeType() == Node.ELEMENT_NODE) {
                        String ingredientId = ingNode.getTextContent();
                                                
                        //Find the ID in the current menu
                        for (MenuItemWrapper m : menu) {
                        	if (m.getId().equals(ingredientId)) {
                        		price += m.getPrice();
                        		ingredientList.add(m);
                        		cont++;
                        	}
                        }
                    }
                }
                
                //Create instance and add it to menu
                CocktailWrapper c = new CocktailWrapper(id, name, price, image, ingredientList);
                cocktails.add(c);
            }
        }
                
        //Append cocktails to menu and return it
        menu.addAll(cocktails);
        return menu;
    }
}