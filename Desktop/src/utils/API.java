package utils;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import model.Drink;

public class API {
	private static final String urlString = "https://drivesafe-201701.appspot.com/api/drink";
		
	public static boolean saveDrink(Drink d, String driverId)
	{
		try {
			String reqParams = "driver_id=" + driverId + "&drink_name=" + d.getName() + "&alcohol=" + d.getAlcohol() + "&ml=" + d.getAmount().toMl() + "&timestamp=" + d.getTimeOrdered();
			int postDataLength = reqParams.getBytes().length;
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
	        conn.setRequestProperty( "charset", "utf-8");
	        conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
	        conn.setUseCaches( false );
	        DataOutputStream wr = new DataOutputStream( conn.getOutputStream());
	        wr.writeBytes( reqParams );
	        wr.flush();
	        wr.close();
	        conn.getResponseCode();
	        
	        //Testing
	        BufferedReader in = new BufferedReader(
			        new InputStreamReader(conn.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	        
		} catch(Exception e) {
			return false;
		}
		return true;
	}

}
