package model;

import java.io.Serializable;

public class Food implements MenuItem, Serializable {
	
	private static final long serialVersionUID = 1L;

	private String name;
	private MenuItemKind kind;
	private double price;
	
	public Food (String name, double price)
	{
		kind = MenuItemKind.FOOD;
		this.name = name;
		this.price = price;
	}
	
	@Override
	public String getName(){
		return this.name;
	}

	@Override
	public MenuItemKind getKind(){
		return this.kind;
	}

	@Override
	public double getPrice(){
		return this.price;
	}
}