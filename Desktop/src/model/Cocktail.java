package model;

import java.io.Serializable;
import java.util.Date;

public class Cocktail extends Drink implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Drink parent;
	private String cocktailName;
	
	public Cocktail(String name, double price, Measure amount, double alcohol, Drink d) {
		super(name, price, amount, alcohol);
		parent = d;
		kind = MenuItemKind.DRINK;
		timeOrdered = new Date();
		
		//Default name
		cocktailName = "Coctel";
	}
	
	@Override
	public Drink getParent() {
		return this.parent;
	}

	@Override
	public double getPrice() {
		return price + parent.getPrice();
	}
	
	@Override
	public Measure getAmount() {
		return new Measure((amount.toMl() + parent.getAmount().toMl()), Unit.ML);
	}
	
	@Override
	public double getAlcohol() {
		return ((this.alcohol * amount.toMl()) + (parent.getAlcohol() * parent.getAmount().toMl())) / getAmount().toMl();
	}
	
	@Override
	public String getName() {
		return cocktailName;
	}
	
	public void setCocktailName(String cocktailName) {
		this.cocktailName = cocktailName;
	}

}
