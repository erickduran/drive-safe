package model;

public interface MenuItemWrapper
{
	public String getId();
	public String getName();
	public double getPrice();
	public String getImage();
}