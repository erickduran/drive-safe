package model;

public class DrinkWrapper implements MenuItemWrapper {
	private String id;
    private String name;
    private double price;
    private double alcohol;
    private double quantity;
    private Unit unit;
    private String image;


    public DrinkWrapper(String id, String name, double price, double alcohol, double quantity, Unit unit, String image)
    {
        this.id = id;
        this.name = name;
        this.price = price;
        this.alcohol = alcohol;
        this.quantity = quantity;
        this.unit = unit;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getAlcohol() {
        return alcohol;
    }
    
    public double getQuantity() {
        return quantity;
    }

    public Unit getUnit() {
        return unit;
    }
    
    public String getImage() {
    		return image;
    }

}
