package model;
public interface MenuItem {
	public double getPrice();
	public String getName();
	public MenuItemKind getKind();
}