package model;

import java.io.Serializable;

public class Measure implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Unit unit;
	private double quantity;

    public Measure (double quantity, Unit unit) 
    {
        this.unit = unit;
        this.quantity = quantity;
    }

    public double toMl() {
        switch(unit) {
            case ML:
                return quantity;
            case OZ:
                return 29.5735 * quantity;
            default:
                return 0;
        }
    }

    public double toOz() {
        switch(unit) {
            case ML:
                return 0.033814 * quantity;
            case OZ:
                return quantity;
            default:
                return 0;
        }
    }
}
