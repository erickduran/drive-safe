package model;

import java.io.Serializable;

public enum MenuItemKind implements Serializable {
	FOOD, DRINK
}
