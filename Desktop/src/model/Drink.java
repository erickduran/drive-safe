package model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Drink implements MenuItem, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	protected String name;
	protected MenuItemKind kind;
	protected double price;
	
	protected Measure amount;
	protected double alcohol; 
	protected Date timeOrdered;
	
	public Drink(String name, double price, Measure amount, double alcohol) {
		this.name = name;
		this.price = price;
		this.amount = amount;
		this.alcohol = alcohol;
		kind = MenuItemKind.DRINK;
		timeOrdered = new Date();
	}
	
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public MenuItemKind getKind() {
		return this.kind;
	}

	@Override
	public double getPrice() {
		return this.price;
	}
	
	public Measure getAmount() {
		return this.amount;
	}
	
	public double getAlcohol() {
		return this.alcohol;
	}
	
	public String getTimeOrdered() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		return df.format(timeOrdered);
	}
	
	public Drink getParent() {
		return null;
	}
	
	public String getIngredientName() {
		return name;
	}
	
	public Measure getIngredientAmount() {
		return this.amount;
	}
	
	public double getIngredientAlcohol() {
		return this.alcohol;
	}
}