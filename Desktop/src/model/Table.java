package model;
import java.io.Serializable;
import java.util.ArrayList;

public class Table implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private ArrayList<Customer> customers;
	private int id;
	
	public Table(int id) {
		this.customers = new ArrayList<Customer>();
		this.id = id;
	}

	public void checkout(Customer c){
		c.checkout();
		customers.remove(c);
	}

	public void addCustomer(Customer c){
		customers.add(c);
	}
	
	public ArrayList<Customer> getCustomers(){
		return customers;
		
	}
	
	public int getSize() {
		return customers.size();
	}
	
	public boolean contains(Customer c) {
		return customers.contains(c);
	}
	
	public boolean removeCustomer(Customer c) {
		boolean status = this.customers.remove(c);
		return status;
	}
	
	public int getId() {
		return this.id;
	}
}