package model;

import java.util.ArrayList;

public class CocktailWrapper implements MenuItemWrapper {
	private ArrayList<MenuItemWrapper> ingredients;
	private String id;
	private double price;
	private String name;
	private String image;
	
	public CocktailWrapper (String id, String name, double price, String image, ArrayList<MenuItemWrapper> ingredients) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.image = image;
		this.ingredients = ingredients;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getPrice() {
		return price;
	}

	@Override
	public String getImage() {
		return image;
	}
	
	public ArrayList<MenuItemWrapper> getIngredients() {
		return ingredients;
	}

}
