package model;

import java.io.Serializable;

public enum Unit implements Serializable{
    ML, OZ;
}
