package model;
import java.io.Serializable;
import java.util.ArrayList;

import utils.API;

public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String description;
	private ArrayList<MenuItem> items;
	private String id;
	
	public Customer(String description) {
		this.description = description;
		items = new ArrayList<MenuItem>();
		id = "";
	}

	//Return false if ID is required, true otherwise
	public boolean order(MenuItem m){
		if (m.getKind().equals(MenuItemKind.DRINK)) {
			if (((Drink)m).getAlcohol() > 0 && !this.isScanned()) {
				return false;
			}
		}
		items.add(m);
		return true;
	}

	//Return the total amount spent by the customer and save alcohol records
	public void checkout(){
		for (MenuItem menuItem : items) {
			if(menuItem.getKind().equals(MenuItemKind.DRINK)) {
				if (((Drink)menuItem).getAlcohol() > 0) {
					API.saveDrink((Drink)menuItem, id);
				}
			}
		}
	}
	
	public double getTotal() {
		double total = 0;
		for (MenuItem menuItem : items) {
			total += menuItem.getPrice();
		}
		return total;
	}
	
	public ArrayList<MenuItem> getItems() {
		return items;
	}
	
	public String getDescription()	{
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public boolean isScanned() {
		if(id == null) {
			return false;
		}
		return (!this.id.equals(""));
	}
	
	public boolean remove(MenuItem m) {
		return items.remove(m);
	}

}