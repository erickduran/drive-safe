var promise = require('bluebird');
var bodyparser = require('body-parser');

var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var creds = require('./db.json')
var connection = `postgres://${creds.username}:${creds.password}@${creds.server}/${creds.database}`;
var db = pgp(connection);

function getDriver(req, res, next) {
  var query = `select * from history where driver_id =\'${req.query.driver_id}\' and drink_time >= (current_timestamp - interval \'2 days\')`;
  db.many(query)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved driver'
        });
    })
    .catch(function (err) {
      res.status(200)
        .json({
          status: 'failed',
          message: err.message
        });
    });
}

function addDrink(req, res, next) {
  var alcohol = parseFloat(req.body.alcohol);
  var ml = parseFloat(req.body.ml);

  query = `insert into history (driver_Id, drink_time, drink_name, alcohol, ml)`+
    `values (\'${req.body.driver_id}\', (TIMESTAMP \'${req.body.timestamp}\'), \'${req.body.drink_name}\', ${alcohol}, ${ml})`;

  db.none(query)
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Inserted drink to driver'
        });
    })
    .catch(function (err) {
      res.status(200)
        .json({
          status: 'failed',
          message: err.message
        });
    });
}


module.exports = {
  getDriver: getDriver,
  addDrink: addDrink
};
