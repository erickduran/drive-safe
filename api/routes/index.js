var express = require('express');
var router = express.Router();

var db = require('../queries');

// ROUTES
router.get('/', function(req, res, next) {
  res.render('index', { title: 'DriveSafe' });
});

router.get('/api/driver', db.getDriver);
router.post('/api/drink', db.addDrink);

module.exports = router;